const path = require('path');
const runCommand = require('./utils/runCommand');

new Promise(resolve => { resolve() })
    .then(() => {
        return runCommand('npm', ['install'], {
            cwd: path.resolve(`${__dirname}/../frontend`),
            env: process.env,
            shell: true
        });
    })
    .then(async () => {
        return runCommand('npm', ['install'], {
            cwd: path.resolve(`${__dirname}/../node`),
            env: process.env,
            shell: true
        });
    })
