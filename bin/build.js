const path = require('path');
const fs = require('fs').promises;
const runCommand = require('./utils/runCommand');

new Promise(resolve => { resolve() })
    .then(() => {
        return runCommand('npm', ['run', 'build'], {
            cwd: path.resolve(`${__dirname}/../frontend`),
            env: process.env,
            shell: true
        });
    })
    .then(async () => {
        await fs.rm(
            path.resolve(`${__dirname}/../node/public`),
            {
                recursive: true,
                    force:  true
            }
        )
    })
    .then(async () => {
        await fs.rename(
            path.resolve(`${__dirname}/../frontend/build`),
            path.resolve(`${__dirname}/../node/public`)
        )
    })
    .then(async () => {
        return runCommand('npx', ['npm-pack-zip'], {
            cwd: path.resolve(`${__dirname}/..`),
            env: process.env,
            shell: true
        });
    })
