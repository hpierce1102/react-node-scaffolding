#!/usr/bin/env node

const path = require('path');
const runCommand = require('./utils/runCommand');

new Promise(resolve => { resolve() })
    .then(() => {
        runCommand('npm', ['run', 'start-live'], {
            cwd: path.resolve(`${__dirname}/../node`),
            env: process.env,
            shell: true
        });
    })
    .then(async () => {
        return runCommand('start', ['chrome', 'http://localhost:3001'], {
            env: process.env,
            shell: true
        });
    })