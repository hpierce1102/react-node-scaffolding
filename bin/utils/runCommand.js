const { spawn } = require( 'child_process' );

function runCommand(command, commandArgs, options) {
    return new Promise((resolve, reject) => {
        const process = spawn(command, commandArgs, options);

        process.stdout.on( 'data', ( data ) => {
            console.log( `stdout: ${ data }` );
        } );

        process.stderr.on( 'data', ( data ) => {
            console.error( `stderr: ${ data }` );
        } );

        process.on( 'close', ( code ) => {
            if (code === 0) {
                resolve()
            } else {
                reject(code);
            }
        } );

        process.on('error', (e) => {
            console.error(e)
        })
    })
}

module.exports = runCommand;