react-node-scaffolding
======================

An example stub application that provides structure for building a React app connected to a node.js backend. The stub 
uses Docker to run the application during development and build tarballs for publication.

Commands
========

Start
-----

```
$ npm run start
```

Initializes a few docker containers, one for the backend, one for the frontend and ensures that both refresh when changes are made.

Install
-------

```
$ npm run install-windows # for Windows hosts
$ npm run install-linux   # for Linux, Mac, whatever. 
```

Goes into each of the code bases and run an `npm install`. This is a necessary step prior to building.

Build
-----

```
$ npm run build-windows # for Windows hosts
$ npm run build-linux   # for Linux, Mac, whatever. 
```

**Protip**: PHPStorm has better support for docker-compose. Instead of using this npm script, try running from the docker-compose.yml directly.

Uses a single Docker container to combine the frontend and the backend and package it into a tarball that can be distributed.
 
-----------------------------

Random Notes:
============

If changes are needed to the Dockerfile, we need to rebuild the images with docker-compose before they will be observed:

```
$ docker-compose build
```