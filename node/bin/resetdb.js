const { getDb } = require('../db/database');
const fs = require('fs').promises;

function getLines(content) {
    let queries = content.toString().split(';');

    queries = queries.map((query) => {
        return query.trim();
    })

    queries = queries.filter((query) => {
        return query.length > 0;
    });

    queries = queries.map((query) => {
        return query + ';';
    })

    return queries;
}

(async function() {
    const db = await getDb();

    const content = await fs.readFile(__dirname + '/default.sql');

    const lines = getLines(content);

    for (let i = 0; i < lines.length; i += 1 ) {
        const query = lines[i];

        if (query.length === 0) {
            continue;
        }

        const runPromise = new Promise((resolve, reject) => {
            db.query(query, (err, results) => {
                if (err) {
                    reject(err);
                }

                resolve(results);
            });
        })

        try {
            console.log(query);
            await runPromise;
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    }

    console.log("Complete!\n");
    process.exit(0);
})()



