const mysql = require('@vlasky/mysql');
const path = require('path');

function initEnvironment() {
    if (process.env.ENVIRONMENT.indexOf('dev') > -1 && process.env.MYSQL_HOST === undefined ) {
        require('dotenv').config({ path: path.resolve(__dirname + '/../../dev.env') });
    }
}

async function getDb() {
    initEnvironment();

    const connection = mysql.createConnection({
        host     : process.env.MYSQL_HOST,
        user     : process.env.MYSQL_USER,
        password : process.env.MYSQL_PASSWORD,
        database : process.env.MYSQL_DATABASE,
    });

    connection.connect();

    return connection;
}

/**
 * @type {*|IDBDatabase}
 */
module.exports = {
    getDb,
};