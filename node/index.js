#!/usr/bin/env node
const { getDb } = require('./db/database');
const express = require('express')
const app = express()
const port = 8080

app.use(express.static('public'));

app.get('/api/foo', async (req, res) => {

    const db = await getDb();
    db.query("SELECT id, info FROM lorem", function(err, results, fields) {
        if (err) {
            console.log(err);
        }

        res.header('Content-Type', 'application/json');
        res.send(JSON.stringify({foo: results}));
    })
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})