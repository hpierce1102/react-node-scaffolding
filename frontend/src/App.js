import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react";

function App() {
  const [foo, setFoo] = useState([])

  useEffect(() => {
    fetch('api/foo')
        .then(response => response.json())
        .then(data => {
          setFoo(data.foo);
        });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <p>Foo:</p>
          {foo.map(item => (
            <span>{item.info}</span>
          ))}
      </header>
    </div>
  );
}

export default App;
